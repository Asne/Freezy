import os
import asyncio
import discord
import random
import psutil
import requests
import sqlite3
import string
import time
import urllib
import json
from discord.ext import commands
from discord.ext.commands import has_permissions, CheckFailure
from discord import Webhook, RequestsWebhookAdapter

intents = discord.Intents.default()
intents.members = True

client = commands.Bot(command_prefix='~', intents=intents)

client.remove_command('help')

@client.event
async def on_ready():
  await client.change_presence(status=discord.Status.online,
    activity=discord.Streaming(name="~help",
    url="https://www.twitch.tv/search?term=Join%20the%20Worldlife%20Project%20now!"))
  print("Freezy has been started.")

@client.event
async def on_member_join(member):
  embed = discord.Embed(title="Instructions", color=0x41b6ff)
  embed.add_field(name="~verify <WID>", value="Verify your WID in the verify channel.", inline=True)
  embed.add_field(name="Don't have a WID?", value="Register by DM'ing Handelshofjüngling#3861.", inline=True)
  dm = await member.create_dm()
  await member.send(embed=embed)

@client.command()
async def verify(ctx, wlid: str):
  usr = ctx.message.author
  userid = usr.id
  db = sqlite3.connect("./database.db")
  db_cursor = db.cursor()
  db_cursor.execute("SELECT * FROM froster WHERE (discordname IS NULL OR discordname = ?) AND id = ?", (userid, wlid))
  ID = wlid
  guild = ctx.guild
  channel = client.get_channel(982302932953366538)
  response = db_cursor.fetchone()
  if response is not None:
    try:
      db_cursor.execute("UPDATE froster SET active = 1 WHERE id = ?", (wlid,))
      db_cursor.execute("UPDATE froster SET discordname = ? WHERE id = ?", (userid, wlid,))
      db.commit()

      db_cursor.execute("SELECT COUNT() FROM froster WHERE active = 1")
      number = db_cursor.fetchone()[0]

      role = discord.utils.get(ctx.guild.roles, name="Registrant")
      dm = await usr.create_dm()
      embed = discord.Embed(title="Welcome to the Worldlife Project!", color=0x41b6ff)
      await usr.add_roles(role)
      await usr.edit(nick=f"№{number}")
      await dm.send(embed=embed)
      await ctx.message.delete()
      return
    except Exception as e:
      dm = await usr.create_dm()
      embed = discord.Embed(title="Something went wrong.", color=0x41b6ff)
      embed.add_field(name="Error", value=f"{e}", inline=True)
      await dm.send(embed=embed)
      await ctx.message.delete()
      return
  dm = await usr.create_dm()
  embed = discord.Embed(title="Invalid WID!", color=0x41b6ff)
  await dm.send(embed=embed)
  await ctx.message.delete()

@client.command()
async def idformat(ctx):
  nums1 = random.randint(1000,9999)
  nums2 = random.randint(1000,9999)
  lets1 = ''.join(random.choice(string.ascii_uppercase) for _ in range(4))
  lets2 = ''.join(random.choice(string.ascii_uppercase) for _ in range(4))
  exampleId = f"WID:{nums1}.{lets1}.{lets2}.{nums2}"
  embed = discord.Embed(title=exampleId, color=0x41b6ff)
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
async def help(message):
  embed = discord.Embed(title="Freezy Help Screen | Prefix: `~`", color=0x41b6ff)
  embed.set_thumbnail(url=client.user.avatar_url)
  embed.set_footer(text="© Copyright Fre3zer 2021-2022")
  embed.add_field(name="help", value="Show this help message.", inline=True)
  embed.add_field(name="ping", value="Show Freezys response time.", inline=True)
  embed.add_field(name="lainwave", value="Get sent a waving Lain.", inline=True)
  embed.add_field(name="clear <NUM>", value="Clear a specified amount of messages.", inline=True)
  embed.add_field(name="rate", value="Get your personal rating by Freezy.", inline=True)
  embed.add_field(name="members", value="Print number of members in current server.", inline=True)
  embed.add_field(name="userinfo <USER>", value="Get all Information about a user.", inline=True)
  embed.add_field(name="serverinfo", value="Get all Information about this server.", inline=True)
  embed.add_field(name="avatar <USER>", value="Get a users avatar.", inline=True)
  embed.add_field(name="peniscalc <USER>", value="Get a users dick size.", inline=True)
  embed.add_field(name="invitebot", value="Invite Freezy to your server.", inline=True)
  embed.add_field(name="invite", value="Get the invite to the Worldlife Projects Server.", inline=True)
  embed.add_field(name="idformat", value="Get an example WID.", inline=True)
  embed.add_field(name="verify <WID>", value="Verify your WID in the verify channel.", inline=True)
  embed.add_field(name="lq", value="Low Quality a user.", inline=True)
  embed.add_field(name="ulq", value="Un-Low Quality a user.", inline=True)
  embed.add_field(name="cool", value="Receive a cool GIF from Freezy.", inline=True)
  embed.add_field(name="genid", value="Generate a new WID in the database.", inline=True)
  embed.add_field(name="setfm <USERNAME>", value="Set your Last.FM Username.", inline=True)
  embed.add_field(name="fm", value="Show current activity status on Last.FM.", inline=True)
  await message.reply(embed=embed, mention_author=False)

@client.command()
async def cool(message):
  embed = discord.Embed(title="", color=0x41b6ff)
  file = discord.File("./res/gifs/cool.gif", filename="cool.gif")
  embed.set_image(url="attachment://cool.gif")
  await message.reply(file=file, embed=embed, mention_author=False)

@client.command()
async def ping(ctx):
  embed = discord.Embed(title="Pong: " + str(round(client.latency*1000)) + "ms", color=0x41b6ff)
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
async def lainwave(message):
  embed = discord.Embed(title="", color=0x41b6ff)
  file = discord.File("./res/lain_wave.png", filename="lain_wave.png")
  embed.set_image(url="attachment://lain_wave.png")
  await message.reply(file=file, embed=embed, mention_author=False)

@client.command(pass_context=True)
@has_permissions(administrator=True, manage_messages=True)
async def clear(ctx, amount=5):
  await ctx.channel.purge(limit=amount)
  embed = discord.Embed(title="Cleared %d messages." %amount, color=0x41b6ff, description=f"Requested by {ctx.author.mention}.")
  await ctx.send(embed=embed, delete_after=3.0)

@client.command()
@has_permissions(manage_roles=True, manage_messages=True)
async def lq(ctx, member : discord.Member):
  role = discord.utils.get(ctx.guild.roles, name="Low Quality")
  if member.id == 689892377871122448 or member.id == 982298228957528084:
    embed = discord.Embed(title="Unable to Low Quality that User.", color=0x41b6ff)
  else:
    await member.add_roles(role)
    embed = discord.Embed(title="Low Quality'd", color=0x41b6ff, description=f"<@{member.id}> is now lower quality.")
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
@has_permissions(manage_roles=True, manage_messages=True)
async def gcm(ctx, member : discord.Member):
  role = discord.utils.get(ctx.guild.roles, name="GCM")
  await member.add_roles(role)
  embed = discord.Embed(title="Welcome to the Golden Circle", color=0xffd700, description=f"<@{member.id}> is now a GCM.")
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
@has_permissions(manage_roles=True, manage_messages=True)
async def ugcm(ctx, member : discord.Member):
  role = discord.utils.get(ctx.guild.roles, name="GCM")
  await member.remove_roles(role)
  embed = discord.Embed(title="Your GC Membership has been revoked.", color=0xffd700, description=f"<@{member.id}> is no longer a GCM.")
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
@has_permissions(manage_roles=True, manage_messages=True)
async def ulq(ctx, member : discord.Member):
  role = discord.utils.get(ctx.guild.roles, name="Low Quality")
  if member.id == 689892377871122448 or member.id == 982298228957528084:
    embed = discord.Embed(title="Unable to Un-Low Quality that User.", color=0x41b6ff)
  else:
    await member.remove_roles(role)
    embed = discord.Embed(title="Un-Low Quality'd", color=0x41b6ff, description=f"<@{member.id}> is no longer lower quality.")
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
@has_permissions(administrator=True, manage_roles=True, manage_messages=True)
async def flush(ctx):
  await ctx.message.delete()
  await ctx.message.channel.send("😳")

@client.command()
@has_permissions(administrator=True, manage_roles=True, manage_messages=True)
async def genid(ctx):
  usr = ctx.message.author
  userid = usr.id
  db = sqlite3.connect("./database.db")
  db_cursor = db.cursor()

  nums1 = random.randint(1000,9999)
  nums2 = random.randint(1000,9999)
  lets1 = ''.join(random.choice(string.ascii_uppercase) for _ in range(4))
  lets2 = ''.join(random.choice(string.ascii_uppercase) for _ in range(4))
  newid = f"WID:{nums1}.{lets1}.{lets2}.{nums2}"

  db_cursor.execute("SELECT * FROM froster WHERE id = ?", (newid,))
  response = db_cursor.fetchone()
  if response is None:
    try:
      db_cursor.execute("INSERT INTO froster (id) VALUES(?)", (newid,))
      db.commit()
      dm = await usr.create_dm()
      embed = discord.Embed(title="Generated new ID.", color=0x41b6ff, description=f"{newid}")
      await dm.send(embed=embed)
      return
    except Exception as e:
      dm = await usr.create_dm()
      embed = discord.Embed(title="Something went wrong.", color=0x41b6ff)
      embed.add_field(name="Error", value=f"{e}", inline=True)
      await dm.send(embed=embed)
      return
  dm = await usr.create_dm()
  embed = discord.Embed(title="WID already in use.", color=0x41b6ff)
  await dm.send(embed=embed)

@client.command()
async def rate(ctx):
  if '<@689892377871122448>' in format(ctx.message.content):
    rate = 100
  else:
    rate = random.randint(0,100)
  embed = discord.Embed(title="I rate you %d/100." %rate, color=0x41b6ff)
  await ctx.reply(embed=embed, mention_author=False)


@client.command()
async def members(ctx):
  amount = ctx.guild.member_count
  embed = discord.Embed(title="%d Members" %amount, color=0x41b6ff)
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
async def avatar(ctx, avatarMember : discord.Member=None):
  if avatarMember != None:
      userAvatarUrl = avatarMember.avatar_url
  else:
      userAvatarUrl = ctx.author.avatar_url
  embed = discord.Embed(title="", color=0x41b6ff)
  embed.set_image(url=userAvatarUrl)
  await ctx.reply(embed=embed, mention_author=False)

@client.command()
async def serverinfo(ctx):
  if ctx.invoked_subcommand is None:
    findbots = sum(1 for member in ctx.guild.members if member.bot)
    embed = discord.Embed(color=0x41b6ff)
    embed.set_thumbnail(url=ctx.guild.icon_url)
    embed.add_field(name="Server Name", value=ctx.guild.name, inline=True)
    embed.add_field(name="Server ID", value=ctx.guild.id, inline=True)
    embed.add_field(name="Members", value=ctx.guild.member_count, inline=True)
    embed.add_field(name="Bots", value=findbots, inline=True)
    embed.add_field(name="Owner", value=ctx.guild.owner, inline=True)
    embed.add_field(name="Created", value=ctx.guild.created_at.strftime("%d.%m.%Y at %H:%M"), inline=True)
    await ctx.reply(embed=embed, mention_author=False)

@client.command()
async def userinfo(ctx, user : discord.Member=None):
  if user is None:
    user = ctx.author
  embed = discord.Embed(color=0x41b6ff)
  embed.set_thumbnail(url=user.avatar_url)
  embed.add_field(name="Full Name", value=user, inline=True)
  embed.add_field(name="Nickname", value=user.nick if hasattr(user, "nick") else "None", inline=True)
  embed.add_field(name="Account Created", value=user.created_at.strftime("%d.%m.%Y at %H:%M"), inline=True)
  embed.add_field(name="Joined Server", value=user.joined_at.strftime("%d.%m.%Y at %H:%M"), inline=True)
  embed.add_field(
    name="Roles",
    value=', '.join([f"<@&{x.id}>" for x in user.roles if x is not ctx.guild.default_role]) if len(user.roles) > 1 else 'None',
    inline=False
  )
  await ctx.reply(embed=embed, mention_author=False)

@client.listen("on_message")
async def mimic(message):
  message.content = message.content.lower()
  if "@everyone" in message.content or "@here" in message.content:
    await message.delete()
  if "👺" in message.content:
    await message.add_reaction("👺")
  if str(client.user.id) in message.content:
    await message.add_reaction("⛄")
  if "discord.gg" in message.content or "discord." in message.content or ".gg/" in message.content:
    if message.author.bot is not True:
      await message.delete()
      dm = await message.author.create_dm()
      embed = discord.Embed(title="Stop advertising!", color=0x41b6ff)
      await dm.send(embed=embed)
  if message.channel.id == 993440039046488146:
    webhook = Webhook.from_url('HELL CHANNEL WEBHOOK', adapter=RequestsWebhookAdapter())
    webhook.send(username=f"{message.author.display_name}", avatar_url=f"{message.author.avatar_url}", content="This message is only visible to Members of the **Golden Circle:tm:**.")
  if "nigger" in message.content or "faggot" in message.content or "allah" in message.content or "nigga" in message.content:
    await message.delete()
    msg = message.content
    if "nigger" in msg:
      msg = msg.replace("nigger", "nikkew")
    if "allah" in msg:
      msg = msg.replace("allah", "jesus")
    if "nigga" in msg:
      msg = msg.replace("nigga", "nikka")
    if "faggot" in msg:
      msg = msg.replace("faggot", "nyaggot")
    if message.channel.id == 982299700348727338:
      webhook = Webhook.from_url('HELL CHANNEL WEBHOOK', adapter=RequestsWebhookAdapter())
    elif message.channel.id == 982320929512226826:
      webhook = Webhook.from_url('MEDIA CHANNEL WEBHOOK', adapter=RequestsWebhookAdapter())
    webhook.send(username=f"{message.author.display_name}", avatar_url=f"{message.author.avatar_url}", content=f"{msg}")

@client.command()
async def invitebot(ctx):
  invite = os.environ['INVITE_URL']
  embed = discord.Embed(title="Invite Freezy", color=0x41b6ff, description=f"[Click here!]({invite})")
  await ctx.reply(embed=embed, mention_author=False)


@client.command()
async def invite(ctx):
  await ctx.reply("https://discord.gg/MFSHPcsNP4", mention_author=False)

@client.command()
async def peniscalc(ctx, *, user : discord.Member = None):
  if user is None:
    user = ctx.author
  random.seed(user.id)
  len = random.randint(0, 20)
  if user.id == 689892377871122448:
    len = 36
  elif user.id == 982298228957528084:
    len = 36
  p = "8" + "="* + len + "D"
  embed = discord.Embed(title=f"{user.display_name}'s dick is this long:", color=0x41b6ff, description=f"{p}")
  await ctx.reply(embed=embed, mention_author=False)


@client.command()
async def setfm(ctx, fmname: str):
  usr = ctx.message.author
  userid = usr.id
  db = sqlite3.connect("./database.db")
  db_cursor = db.cursor()
  db_cursor.execute("SELECT * FROM lastfm WHERE (userid IS NULL OR userid = ?) AND fmname = ?", (userid, fmname))
  response = db_cursor.fetchone()
  if response is None:
    try:
      db_cursor.execute("INSERT INTO lastfm (userid, fmname) VALUES(?, ?)", (userid, fmname))
      db.commit()
      embed = discord.Embed(title="Success!", color=0x41b6ff, description=f"Your Last.FM Username was set to {fmname}.")
      await ctx.reply(embed=embed, mention_author=False)
      return
    except Exception as e:
      dm = await usr.create_dm()
      embed = discord.Embed(title="Something went wrong", color=0x41b6ff)
      embed.add_field(name="Error", value=f"{e}", inline=True)
      await dm.send(embed=embed)
      return
  if response is not None:
    try:
      db_cursor.execute("UPDATE lastfm SET fmname = ? WHERE userid = ?", (fmname, userid))
      db.commit()
      embed = discord.Embed(title="Success!", color=0x41b6ff, description=f"Updated your Last.FM Username to {fmname}.")
      await ctx.reply(embed=embed, mention_author=False)
      return
    except Exception as e:
      dm = await usr.create_dm()
      embed = discord.Embed(title="Something went wrong", color=0x41b6ff)
      embed.add_field(name="Error", value=f"{e}", inline=True)
      await dm.send(embed=embed)
      return

@client.command()
async def fm(ctx, *, user : discord.Member = None):
  if user is None:
    user = ctx.author
  apikey = os.environ['LASTFMKEY']
  db = sqlite3.connect("./database.db")
  db_cursor = db.cursor()
  db_cursor.execute(f"SELECT * FROM lastfm WHERE userid='{user.id}'")
  response = db_cursor.fetchone()
  if response is None:
    embed = discord.Embed(title="Please set your Username first.", color=0x41b6ff, description="Use `~setfm <USERNAME>`.")
    await ctx.reply(embed=embed, mention_author=False)
  async with ctx.channel.typing():
    try:
      url_song = f'http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&nowplaying="true"&user={response[1]}&api_key={apikey}&format=json'
      data_song = urllib.request.urlopen(url_song).read().decode()
      obj_song = json.loads(data_song)

      song_image = obj_song['recenttracks']['track'][0]['image'][3]['#text']
      song_artist = obj_song['recenttracks']['track'][0]['artist']['#text']
      song_artist_url = f"https://www.lastfm/music/" + song_artist.replace(" ", "+")
      song_name = obj_song['recenttracks']['track'][0]['name']
      song_album = obj_song['recenttracks']['track'][0]['album']['#text']
      song_url = obj_song['recenttracks']['track'][0]['url']

      url_user = f'https://ws.audioscrobbler.com/2.0/?method=user.getinfo&user={response[1]}&api_key={apikey}&format=json'
      data_user = urllib.request.urlopen(url_user).read().decode()
      obj_user = json.loads(data_user)

      user_profilepic = obj_user['user']['image'][0]['#text']
      user_playcount = obj_user['user']['playcount']

      embed = discord.Embed(color=0x41b6ff)
      embed.set_author(name=f"{response[1]}:", icon_url=user_profilepic, url=f"https://www.last.fm/user/{response[1]}")
      embed.set_thumbnail(url=song_image)
      embed.add_field(name="Track", value=f"[{song_name}]({song_url})", inline=True)
      embed.add_field(name="Artist", value=f"[{song_artist}]({song_artist_url})", inline=True)
      embed.set_footer(text=f"Total Scrobbles: {user_playcount}/ Album: {song_album}")
      await ctx.reply(embed=embed, mention_author=False)
      return
    except Exception as e:
      dm = await ctx.message.author.create_dm()
      embed = discord.Embed(title="Something went wrong", color=0x41b6ff)
      embed.add_field(name="Error", value=f"{e}", inline=True)
      await dm.send(embed=embed)
      return

@client.command()
async def fortune(ctx):
  with open('./Fortune.DD') as datei:
    zeilen = [line for line in datei]
    auswahl = random.choice(zeilen)
  await ctx.reply(auswahl, mention_author=False)

client.run(os.environ['DISCORD_TOKEN'])