{ pkgs ? import <nixpkgs> {} }:

let
  pythonBase = pkgs.python39;
  pythonResult = pythonBase.withPackages (p: with p; [
    discordpy
    psutil
    utils
    requests
    urllib3
    json5
  ]);
in
pkgs.mkShell {
  buildInputs = [
    pythonResult
    pkgs.sqlitebrowser
  ];
  DISCORD_TOKEN = "YOUR TOKEN HERE";
  PYTHONPATH = "${pythonResult}/${pythonResult.sitePackages}";
  INVITE_URL = "YOUR INVITE URL HERE";
  LASTFMKEY = "YOUR LASTFM KEY HERE";
  LASTFMSECRET = "YOUR LASTFM SECRET HERE";
  FLAKEY_INVITE_URL = "FLAKEY INVITE URL HERE";
  FLAKEY_DISCORD_TOKEN = "FLAKEY TOKEN HERE";
}
